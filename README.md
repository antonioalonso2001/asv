# UEFA  Champions league 2018

Ésta aplicación de despliega usando docker-compose para lo cual la máquina donde se despliegueha de tener instalago **Docker** y **docker-compose**.

En el despliegue de la aplicación se accede a los 2 repositorios donde se encuentran el back (desarrollado en Laravel) y el front (desarrollado en Angular)

**Backend:** git clone https://antonioalonso2001@bitbucket.org/antonioalonso2001/backend-asv.git

**Frontend:** git clone https://antonioalonso2001@bitbucket.org/antonioalonso2001/frontend-asv.git

## Como instalarlo

Lo primero que hacemos es descargarnos el repositorio:

```
git clone https://antonioalonso2001@bitbucket.org/antonioalonso2001/asv.git
```
Después entramos en el directorio **asv** recién creado y ejecutamos:

```
docker-compose up
```

En la primera instalación el proceso es un poco largo ya que requiere descargarse las imágenes de los contenedores necesarios y construir una imagen an medida para **php-fpm**

En el proceso de instalación se ejecuta un script que hace el deploy tanto del backend como el front. Una vez finalizado se puede acceder a ellos en la ssiguientes URL:

**backend:** http://localhost:8080

**frontend:** http://localhost


