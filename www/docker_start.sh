#!/bin/bash



# the backend
cd /var/www
if test "$(ls -A "/var/www/backend")"; then
    echo "Repository already downloaded"
else
   git clone https://antonioalonso2001@bitbucket.org/antonioalonso2001/backend-asv.git backend
fi

cd backend


if test "$(ls -A "/var/www/backend/.env")"; then
    echo "DDBB migrated"
    composer update
else
#waith for mysql

   cp ../.env .env
   composer install

   php artisan key:generate
   until php artisan migrate:refresh --force --seed; do
	>&2 echo "Mysql is unavailable - sleeping"
	sleep 10
   done
fi




chown -R www-data:www-data /var/www/backend

#now the front
cd /var/www
git clone https://bitbucket.org/antonioalonso2001/frontend-asv.git frontend

cd frontend

npm install -g @angular/cli

npm install

ng build --prod

echo "######################################################"
echo "DONE!"
echo "Accede a http://localhost para acceder a la aplicación"

